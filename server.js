 const { request } = require('express');
const express = require('express');

  // mongoose is a package that allows us to create Schema to
  // Model our data structures and to manipulate our database
  // using different access methods.
 const mongoose = require('mongoose');

 const port = 3001;
 const app = express();

 // [Section] MongoDB connection
 // syntax:
 /** 
   mongoose.connect("mongoDBconnectionString", {
    options to avoide errors in our connection
   })
  * */ 

   mongoose.connect("mongodb+srv://admin:admin@batch245-espiritu.dgm8gby.mongodb.net/s35-discussion?retryWrites=true&w=majority", 

        // Allows us to avoid any current and future errors while connecting to mongoDB
        {
            useNewUrlParser:true,
            useUnifiedTopology: true
        }
   );

   let db = mongoose.connection;

    // erro handling in connecting
    db.on("error", console.error.bind(console, "Connection error"));


    // This will be triggered if the connection is succesfull
    db.once("open", ()=> console.log("we're connected to the cloud database!"));




        // Mongoose Schema
            // Schema determine the structures of the documents to be written in the databse
            // Schema act as the blueprint to our data

            //Syntax:
            /**
                const schemaName = new mongoose.Schema({keyvaluepairs})
             */

        // taskSchema it contains two properties: name & status
        // required is used to specify a field must not be empty
            const taskSchema = new mongoose.Schema(
                {
                    name:{
                        type:String,
                        required:[true, "Task name is required!"]
                    },
                    status:{
                        type:String,
                        default:"pending"
                    }
                }
            )


    // [Section] - Models
    /**
        Users schema to create/instatiate documents/objects that follows our Schema
        structure

        The variable/object that will be created can be used to run commands
        with our database

        syntax:
        const variableName = mongoose.model("collectionName", schemasName);
     */

        const Task = mongoose.model("Task", taskSchema)
      

// middlewares
app.use(express.json()) // Allows the app to read json data
app.use(express.urlencoded({extended:true})) // it will allow our app to read from forms.



// [Section] - Routing
 /*
    Create/add new task
    1. Check if the task is existing.
        - if task aready exist return a message
        'task is already existing!'
        - if the task doesn't exist in the 
        database, we will add it in the database
 */

    app.post("/tasks", (request, response) => {
        let input = request.body

       console.log(input.status);
       if(input.status === undefined){
        Task.findOne({name:input.name}, (error, result) => {
            console.log(result);

            if(result !==null){
                return response.send("The task is already existing!")
            }
            else{
                let newTask = new Task({
                    name: input.name
                })

                /*
                    save() method will save the object in the 
                    collection that the object instatiated
                */
                newTask.save( (saveError, savedTask)=>{
                    if(saveError){
                        return console.log(saveError);
                    }
                    else {
                        return response.send("New Task created!")
                    }
                })
            }
        })

       }else{
        Task.findOne({name:input.name}, (error, result) => {
            console.log(result);

            if(result !==null){
                return response.send("The task is already existing!")
            }
            else{
                let newTask = new Task({
                    name: input.name,
                    status: input.status
                })

                /*
                    save() method will save the object in the 
                    collection that the object instatiated
                */
                newTask.save( (saveError, savedTask)=>{
                    if(saveError){
                        return console.log(saveError);
                    }
                    else {
                        return response.send("New Task created!")
                    }
                })
            }
        })
       }

    })


    app.get("/tasks", (request, response) =>{
        Task.find({}, (error, result) =>{
            if(error){
                console.log(error);
            }else{
                response.send(result)
            }
        })
    })





    // ====================== for the activity ========================

    // users Schema
    const usersSchema = new mongoose.Schema(
        {
            userName:{
                type:String,
                required:[true, "User name is required!"]
            },
            password:{
                type:String,
                default:""
            }
        }
    )


    // users Model
    const User = mongoose.model("User", usersSchema)

    app.post("/signup", (request, response)=>{
        let input = request.body

        if(input.password === undefined){
            User.findOne({userName:input.userName}, (error, result) => {
                console.log(result);
    
                if(result !==null){
                    return response.send("The user already exist!")
                }
                else{
                    let newUser = new User({
                        userName: input.userName,
                        
                    })
    
                    /*
                        save() method will save the object in the 
                        collection that the object instatiated
                    */
                    newUser.save( (saveError, savedTask)=>{
                        if(saveError){
                            return console.log(saveError);
                        }
                        else {
                            return response.send("New User registered")
                        }
                    })
                }
            })
        }
        
        else{
            User.findOne({userName:input.userName}, (error, result) => {
                console.log(result);
    
                if(result !==null){
                    return response.send("The user already exist!")
                }
                else{
                    let newUser = new User({
                        userName: input.userName,
                        password: input.password
                    })
    
                    /*
                        save() method will save the object in the 
                        collection that the object instatiated
                    */
                    newUser.save( (saveError, savedTask)=>{
                        if(saveError){
                            return console.log(saveError);
                        }
                        else {
                            return response.send("New User registered")
                        }
                    })
                }
            })
        }
    })



    app.listen(port, ()=> console.log(`Server is running at port: ${port}!`))